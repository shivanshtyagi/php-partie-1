 <!DOCTYPE html>
<html>
<body>

<h1>Exercise 1</h1>
<?php
//Créer une variable et l'initialiser à 0. Tant que cette variable n'atteint pas 10
$a = 0;
while ($a == 10){
echo $a;
}
?>

<h1>Exercise 2</h1>
<?php
//Créer deux variables. Initialiser la première à 0 et la deuxième avec un nombre compris en 1 et 100. Tant que la première variable n'est pas supérieur à 20     
$i = 0;     
$k = 2;       
while ($i <= 20){         
$j = $i*$k;     
echo $j;     
$i++;         
}  
?>

<h1>Exercise 3</h1>
<?php
//Créer deux variables. Initialiser la première à 100 et la deuxième avec un nombre 
//compris en 1 et 100. Tant que la première variable n'est pas inférieur ou égale à 10
$i = 100;     
$k = 20;       
while (!($i <= 10)){         
    $j = $i*$k;     
    echo $j;     
    $i--;         
}  
?>

<h1>Exercise 4</h1>
<?php
//Créer une variable et l'initialiser à 1. Tant que cette variable n'atteint pas 10 
//l'afficher,    l'incrementer de la moitié de sa valeur

$i = 1;            
while ($i <= 10){         
    echo $i;
    $i = $i + $i/2;     
    echo "<br/>";              
}  
?>


<h1>Exercise 5</h1>
<?php

//En allant de 1 à 15 avec un pas de 1, afficher le message On y arrive presque...

for ($i = 1; $i <= 10; $i++)  {
    echo $i;
    echo "<br/>"; 
    echo "on y arrive presque......";

}
?>

<h1>Exercice 6</h1>
<?php

//En allant de 20 à 0 avec un pas de 1, afficher le message C'est presque bon...



for ($i = 20; $i >= 0; $i--)  {
    echo "<br/>";
    echo $i;
    echo "<br/>"; 
    echo "its almost good......";

}
?>

<h1>Exercice 7</h1>
<?php

// En allant de 1 à 100 avec un pas de 15, afficher le message On tient le bon bout...

for($i=1; $i<=100; $i+=15){
    echo "<br/>";
    echo $i;
    echo "<br/>";
    echo "on tien le bon bout......";
}

?>

<h1>Exercice 8</h1>
<?php

// En allant de 200 à 0 avec un pas de 12, afficher le message Enfin ! ! !

for($i=200; $i>=0; $i-=12){
    echo "<br/>";
    echo $i;
    echo "<br/>";
    echo "Enfin......";
}


?>


</body>
</html> 